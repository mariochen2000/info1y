//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int fib_ric (int num){
    
    if(num == 1 || num == 2)
        return 1;
    
    else
        return (fib_ric(num-1) + fib_ric(num-2));
    
}

int main() {

    int num = 0;
    
    printf("Inseret value: ");
    scanf("%d", &num);
    
    printf("f(%d) = %d\n",num,fib_ric(num));

    return 0;

}
