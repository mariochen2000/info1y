#include "liste_int.h"

/* VERSIONI ITERATIVE/RICORSIVE DA IMPLEMENTARE */
int lunghezza_ric(lista l);
int lunghezza_iter(lista l);

lista ricerca_ric(lista l, data el);
lista ricerca_iter(lista l, data el);

lista inserisci_in_coda_ric (lista l, data el);
lista inserisci_in_coda_iter (lista l, data el);

lista rimuovi_in_coda_ric (lista l);
lista rimuovi_in_coda_iter (lista l);

lista rimuovi_ric (lista l, data el);
lista rimuovi_iter (lista l, data el);

void  stampa_ric (lista l);
void  stampa_iter (lista l);

/*************************************************/

int lunghezza (lista l, int ric)
{
  if (ric==1)
    return lunghezza_ric(l);
  else
    return lunghezza_iter(l);
}

lista ricerca (lista l, data el, int ric)
{
  if (ric==1)
    return ricerca_ric(l,el);
  else
    return ricerca_iter(l,el);
}

lista inserisci_in_testa(lista l, data el)
{
  lista temp = malloc(sizeof(struct nodo));
  temp->el = el;
  temp->next = l;
  return temp;
}

lista inserisci_in_coda (lista l, data el, int ric)
{
  if (ric==1)
    return inserisci_in_coda_ric(l,el);
  else
    return inserisci_in_coda_iter(l,el);
}

lista rimuovi_in_testa (lista l)
{
  if (l!=NULL)
  {
    lista temp = l;
    l = l->next;
    free(temp);
  }
  return l;
}

lista rimuovi_in_coda (lista l, int ric)
{
  if (ric==1)
    return rimuovi_in_coda_ric(l);
  else
    return rimuovi_in_coda_iter(l);
}

lista rimuovi (lista l, data el, int ric)
{
  if (ric==1)
    return rimuovi_ric(l,el);
  else
    return rimuovi_iter(l,el);
}

void stampa (lista l, int ric)
{
  if (ric==1)
    stampa_ric(l);
  else
    stampa_iter(l);
}

int lunghezza_ric(lista l)
{
  if (l==NULL)
    return 0;
  else
    return 1 + lunghezza_ric(l->next);
}

int lunghezza_iter(lista l)
{
  int lung=0;
  while (l!=NULL)
  {
    lung++;
    l = l->next;
  }
  return lung;
}

lista ricerca_ric(lista l, data el)
{
  if (l == NULL || l->el == el)
      return l;
  else
    return ricerca_ric(l->next,el);
}

lista ricerca_iter(lista l, data el)
{
  while (l!=NULL && l->el!=el)
    l = l->next;

  return l;
}

lista inserisci_in_coda_ric (lista l, data el)
{
  if (l == NULL)
    return inserisci_in_testa(l,el);
  else
  {
    l->next = inserisci_in_coda_ric(l->next,el);
    return l;
  }
}

lista inserisci_in_coda_iter (lista l, data el)
{

  if(l == NULL)
    return inserisci_in_testa(l,el);
  else
  {
    lista cur = l;
    while (cur->next != NULL)
      cur = cur->next;
    cur->next = inserisci_in_testa(cur->next,el);
    return l;
  }
}

lista rimuovi_in_coda_ric (lista l)
{
  if (l!=NULL)
  {
    if (l->next == NULL)
      return rimuovi_in_testa(l);
    else
      l->next = rimuovi_in_coda_ric(l->next);
  }
  return l;
}

lista rimuovi_in_coda_iter (lista l)
{
  if (l!=NULL)
  {
    lista cur = l;
    lista pre = NULL;
    while (cur->next !=NULL)
    {
      pre = cur;
      cur = cur->next;
    }
    if (pre!=NULL)
      pre->next = NULL;
    else
      l = NULL;
    free (cur);
  }
  return l;
}

lista rimuovi_ric (lista l, data el)
{
  if (l!=NULL)
  {
    if (l->el == el)
    {
      lista tmp = l;
      l = l->next;
      free(tmp);
    }
    else
      l->next = rimuovi_ric(l->next,el);
  }
  return l;
}

lista rimuovi_iter (lista l, data el)
{
  lista pre=NULL, cur=l;
  while (cur!=NULL && cur->el != el)
  {
    pre = cur;
    cur = cur->next;
  }

  if (cur!=NULL)
  {
    pre->next = cur->next;
    free(cur);
  }

  return l;
}

void  stampa_ric (lista l)
{
  if (l==NULL)
    printf("END\n");
  else
  {
    printf(STAMPA,l->el);
    stampa_ric(l->next);
  }
}

void  stampa_iter (lista l)
{
  while (l!=NULL)
  {
    printf(STAMPA,l->el);
    l = l->next;
  }
  printf("END\n");
}
